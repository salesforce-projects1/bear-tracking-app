import { publish, MessageContext } from 'lightning/messageService';
import BEAR_LIST_UPDATE_MESSAGE from '@salesforce/messageChannel/BearListUpdate__c';
import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, wire } from 'lwc';
/** BearController.searchBears(searchTerm) Apex method */
import searchBears from '@salesforce/apex/BearController.searchBears';
export default class BearList extends NavigationMixin(LightningElement) {
	searchTerm = '';
	bears;
  @wire(MessageContext) messageContext;
  @wire(searchBears, {searchTerm: '$searchTerm'})
  loadBears(result) {
    this.bears = result;
    if (result.data) {
      const message = {
        bears: result.data
      };
      publish(this.messageContext, BEAR_LIST_UPDATE_MESSAGE, message);
    }
  }
	handleSearchTermChange(event) {
		// Debouncing this method: do not update the reactive property as
		// long as this function is being called within a delay of 300 ms.
		// This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchTerm = event.target.value;
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.searchTerm = searchTerm;
		}, 300);
	}
	get hasResults() {
		return (this.bears.data.length > 0);
	}
	handleBearView(event) {
		// Get bear record id from bearview event
		const bearId = event.detail;
		// Navigate to bear record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: bearId,
				objectApiName: 'Bear__c',
				actionName: 'view',
			},
		});
	}
}
/*
We’ve greatly simplified the JS code by decorating our bears property
with wired Apex. All the data we need is now coming through this single line:

@wire(getAllBears) bears;

Code highlights:
We add a searchTerm reactive property and we pass it as a parameter of our wired Apex
call to searchBears.
The handleSearchTermChange function is used to react to changes in the value of the 
search input field. We purposefully introduce a 300 millisecond delay when updating 
the searchTerm reactive property. If an update is pending, we cancel it and reschedule 
a new one in 300 ms. This delay reduces the number of Apex calls when the user is typing
letters to form a word. Each new letter triggers a call to handleSearchTermChange but 
ideally, searchBears is only called once when the user has finished typing. 
This technique is called debouncing.
We expose the hasResults expression to check whether our search has returned bears.

/* 
Imperitive Apex Code


import { LightningElement } from 'lwc';
import ursusResources from '@salesforce/resourceUrl/ursus_park'; */
/* BearController.getAllBears() Apex method */
/*
import getAllBears from '@salesforce/apex/BearController.getAllBears';
export default class BearList extends LightningElement {
	bears;
	error;
	appResources = {
		bearSilhouette: `${ursusResources}/img/standing-bear-silhouette.png`,
	};
	connectedCallback() {
		this.loadBears();
	}
	loadBears() {
		getAllBears()
			.then(result => {
				this.bears = result;
			})
			.catch(error => {
				this.error = error;
			});
	}
}
*/
/*
Code highlights:
We import the ursusResources adapter, which gives us access to a static resource associated with our app.
We use this adapter to build an appResources object that exposes the bear silhouette image URL in the template.
We import the getAllBears adapter, which allows us to interact with the BearController.getAllBears() Apex 
method. The BearController class is bundled in the code that you deployed at the beginning of this project.
The getAllBears method returns the result of a query that fetches all bear records.
We implement the connectedCallback function, which allows us to execute code after the component is loaded. 
We use this function to call the loadBears function.
The loadBears function calls the getAllBears adapter. The adapter calls our Apex code and returns a JS promise. 
We use the promise to either save the returned data in the bears property or to report errors. Retrieving data
 using this approach is called imperative Apex.
*/